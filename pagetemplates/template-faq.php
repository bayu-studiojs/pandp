<?php /* Template Name: FAQ Page Template */ get_header(); ?>

	<div class="body-section"> 
      <div class="contact-content-div1180 w-row">
        <div class="post-left-col w-col w-col-2"></div>
        <div class="featured-post-col w-col w-col-10">
       
		  
		  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<?php $faqItms = get_field('faq_section');
			if($faqItms) { ?>
				<div class="faq-questions-parent">
					<?php foreach($faqItms as $key => $value) { ?>
						<a class="faq-question" data-div="itm-<?php echo $key;?>" href="javascript:void(0);">
							<span class="faq-arrow">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</span>
							<?php echo $value['question'];?>
						</a>
					<?php } ?>
				</div>
				<div>
				<?php foreach($faqItms as $key => $value) { ?>
				<div class="faq-answer-parent itm-<?php echo $key;?>" data-ix="fade-on-scroll">
					<h4><?php echo $value['question'];?></h4>
					<?php echo $value['answer'];?>
				</div>
				<?php } 
				echo "</div>";
			}
		endwhile; 
		endif; ?>
		
		 
        </div>
      </div>
    </div>

<?php get_footer(); ?>
