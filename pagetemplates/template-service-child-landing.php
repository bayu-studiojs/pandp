<?php
 /* Template Name: Service Child Landing Page*/
 get_header(); ?>


  <div class="whitesection">
      <div class="containerdiv">
        <div class="whitetext2col">
          <div class="whitetextcolumn">
            <?php 
				if (have_posts()): while (have_posts()) : the_post(); ?>
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
					</article>
					<!-- /article -->
					<?php endwhile; ?>
					<?php else: ?>
					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
					</article>
					<!-- /article -->
					<?php endif; ?>
          </div>
          <div class="whitetextcolumn">
			<?php 
			$mdaType = get_field('media_type');
			if($mdaType=='image' && get_field('image')!='') { ?>
				<img src="<?php echo get_field('image')?>" />
			<?php } else if($mdaType=='video' && get_field('video')!='') {
				$video_url = get_field('video');
				if($video_url!=''){
					$videourl = $video_url;
				}else{
					$videourl = "https://player.vimeo.com/video/311814519?color=ff9933&title=0&byline=0&portrait=0";
				} ?>
				<div class="videoembedd w-embed w-iframe w-script">
				  <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="<?php echo $videourl;?>" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
				  <script src="https://player.vimeo.com/api/player.js"></script>
				</div>
			<?php } ?>
          </div>
        </div>
      </div>
    </div>
	<?php $btnSec = get_field('button_section'); 
	if($btnSec) { ?>
		<div class="ctasection">
			<div class="registernowctaphone">
				<?php if(isset($btnSec['heading']) && $btnSec['heading']!='') { ?>
					<h3><?php echo $btnSec['heading']; ?></h3>
				<?php } 
				$btnTxt = 'Register Now';
				if(isset($btnSec['text']) && $btnSec['text']!='') {
					$btnTxt = $btnSec['text'];
				} ?>
				<a href="#footerregistration" class="ctasubmitbutton footer w-button"><?php echo $btnTxt;?></a>
			</div>
		</div>
	<?php } ?>
      <div class="highlightssection">
		<div class="containerdiv">
			<?php $hlheading = get_field('highlight_heading');
			if($hlheading!=''){ ?>
			<h2 class="h2centred"><?php echo $hlheading; ?></h2>
		  <?php } ?>
		  <div class="w-layout-grid highlightcardsparent">
			<?php $smsfhighlightslists = get_field('highlight_item');
			if($smsfhighlightslists!=''){
				$gg = 1;
				foreach($smsfhighlightslists as $smsfhighlightslist){ ?>
					<div id="grid<?php echo $gg;?>" class="highlightcard">
						<?php if(isset($smsfhighlightslist['icon']) && $smsfhighlightslist['icon']!='') { ?>
							<img src="<?php echo $smsfhighlightslist['icon']; ?>" width="120" alt="" class="cardicon">
						<?php } 
						if(isset($smsfhighlightslist['title']) && $smsfhighlightslist['title']!='') { ?>
							<h3 class="cardh3"><?php echo $smsfhighlightslist['title']; ?></h3>
						<?php } 
						if(isset($smsfhighlightslist['description']) && $smsfhighlightslist['description']!='') { ?>
							<div><?php echo $smsfhighlightslist['description']; ?></div>
						<?php } ?>
					</div>
				<?php
				  $gg++;
				}
			} ?> 
			
			
		  </div>
		</div>
  </div>  
<div class="footercta">
	<div class="w-container">
		<h2 class="h2centred h2orange"><?php echo get_field('cta_heading'); ?></h2>
		<div id="footerregistration"><?php echo get_field('description'); ?></div>
		<?php if($btnSec) { ?>
		<div class="footerctadesktop">
			<?php if(isset($btnSec['heading']) && $btnSec['heading']!='') { ?>
				<h3><?php echo $btnSec['heading']; ?></h3>
			<?php } 
			$btnTxt = 'Register Now';
			if(isset($btnSec['text']) && $btnSec['text']!='') {
				$btnTxt = $btnSec['text'];
			} ?>
			<a href="#contact" class="ctasubmitbutton footer w-button"><?php echo $btnTxt;?></a>
		</div>
		<?php } 
		$frmHdng = get_field('form_heading');
		$formId = get_field('form_item'); ?>
		<div class="footerctaparent">
			<div class="ctadiv footerregistration">
				<?php if($frmHdng) { ?>
					<h2 class="ctah"><?php echo $frmHdng; ?></h2>
				<?php } 
				if($formId) { ?>
					<div class="w-form">
						<?php echo do_shortcode('[gravityform id="'.$formId.'" title="false" description="false"]');?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>