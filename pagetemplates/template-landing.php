<?php /* Template Name: Landing Page Template */ get_header(); ?>
<div class="body-section">
	<div class="post-content-div1180 w-row">
		<div class="w-col w-col-12">
			<main role="main">
				<!-- section -->
				<section>
					<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
					</article>
					<!-- /article -->
					<?php endwhile; ?>
					<?php else: ?>
					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
					</article>
					<!-- /article -->
					<?php endif; ?>
				</section>
				<!-- /section -->
			</main>
		</div>
	</div>
	
</div>

<?php while ( have_rows( 'page_builder' ) ) : the_row(); 
	get_template_part( 'partials/block', get_row_layout() ); 
endwhile;?>

<?php //get_template_part( 'partials/block', 'contact_home' ); ?>
<?php get_footer(); ?>