<?php
 /* Template Name: SMSF Landing Page*/
 get_header(); ?>


  <div class="whitesection">
      <div class="containerdiv">
        <div class="whitetext2col">
          <div class="whitetextcolumn">
            <?php 
				if (have_posts()): while (have_posts()) : the_post(); ?>
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
					</article>
					<!-- /article -->
					<?php endwhile; ?>
					<?php else: ?>
					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
					</article>
					<!-- /article -->
					<?php endif; ?>
          </div>
          <div class="whitetextcolumn">
			<?php 
			
			$video_url = get_field('video_url');
			if($video_url!=''){
				$videourl = $video_url;
			}else{
				$videourl = "https://player.vimeo.com/video/311814519?color=ff9933&title=0&byline=0&portrait=0";
			}
			
			
			?>
			
            <div class="videoembedd w-embed w-iframe w-script">
              <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="<?php echo get_field('video_url');?>" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
              <script src="https://player.vimeo.com/api/player.js"></script>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ctasection">
      <div class="registernowctaphone">
        <h3><?php echo get_field('book_for_free_heading', 'option'); ?></h3><a href="#footerregistration" class="ctasubmitbutton footer w-button">Register Now</a></div>
    </div>
	
	
	  <div class="highlightssection">
		<div class="containerdiv">
			<?php 
			
			$smsf_hlheading = get_field('smsf_highlights_heading');
			if($smsf_hlheading!=''){
				$smsfhlheading = $smsf_hlheading;
			}else{
				$smsfhlheading = "SMSF property investment can have great rewards:";
			}
			
			
			?>
			
		  <h2 class="h2centred"><?php echo $smsfhlheading; ?></h2>
		  <div class="w-layout-grid highlightcardsparent">
			
			<?php 
			
			$smsfhighlightslists = get_field('smsf_highlights');
			if($smsfhighlightslists!=''){
				$gg = 1;
				foreach($smsfhighlightslists as $smsfhighlightslist){
						
					?>
					
						<div id="grid<?php echo $gg;?>" class="highlightcard">
							<img src="<?php echo $smsfhighlightslist['sec_icon']; ?>" width="120" alt="" class="cardicon">
							  <h3 class="cardh3"><?php echo $smsfhighlightslist['sec_title']; ?></h3>
							  <div><?php echo $smsfhighlightslist['sec_details']; ?></div>
						</div>
					
				<?php
				
				  $gg++;
				}
				
			}
			
			?> 
			
			
		  </div>
		</div>
  </div>  



<?php get_template_part( 'partials/block', 'smsf' ); ?>
<?php get_footer(); ?>