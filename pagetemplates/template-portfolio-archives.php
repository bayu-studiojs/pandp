<?php /* Template Name: Portfolio - Investment Archives Template */ get_header(); ?>
<?php
$focusproperties = get_field('please_pick_your_properties','option');
if(false && $focusproperties!=''){ //Hide this section
	echo '<div class="body-section featured-prop-section">';
		echo '<div class="post-content-div1180 w-row">';
			echo '<div class="post-left-col w-col w-col-2 w-col-stack"><h2 data-ix="fade-on-load-3" >Property In Focus</h2></div>';
				echo '<div class="featured-post-col w-col w-col-10 w-col-stack">';
	
					$excludeIds = array();
					foreach($focusproperties as $focusproperty){
							
							//echo $focusproperty->ID;
							$excludeIds[] = $focusproperty->ID;
							$proVdo = get_field('video_url',$focusproperty->ID);
							if($proVdo) {
								if(!empty(get_field("property_video_image",$focusproperty->ID))){
									$property_image = get_field("property_video_image",$focusproperty->ID);
								}else{
									$property_image = get_template_directory_uri().'/img/597836b3ffd6d400010641c3_blog-thumb-3@2x.jpg';
								}
								$vdoId = (int) substr(parse_url($proVdo, PHP_URL_PATH), 1);
								$ncodVdo = rawurlencode($proVdo);
								$ncodVdoThumb = rawurlencode($property_image);
								$plyrUrl = 'https://player.vimeo.com/video/'.$vdoId;
								$ncodPlyrUrl = rawurlencode($plyrUrl);
							} else {
								$propImgArr = wp_get_attachment_image_src(get_post_thumbnail_id($focusproperty->ID),'full');
								if(isset($propImgArr[0]) && $propImgArr[0]!='') {
									$property_image = $propImgArr[0];
								} else {
									$property_image = get_template_directory_uri().'/img/597836b3ffd6d400010641c3_blog-thumb-3@2x.jpg';
								}
							}
					
					echo '<div class="featured-post-row featured-prop-row w-row" data-ix="fade-on-load-3">';
						echo '<div class="column w-col w-col-6 w-col-stack">
								<div class="featured-prop-thumb featured-thumb" style="background-image: url(\''.$property_image.'\')">';
									if($proVdo) {?>
									<a class="play-icon-video w-lightbox" href="#"><i class="fa fa-play-circle" aria-hidden="true"></i>
										<script class="w-json" type="application/json">{ "items": [{
											"type": "video",
											"url": "<?php echo $proVdo;?>",
											"html": "<iframe class=\"embedly-embed\" src=\"//cdn.embedly.com/widgets/media.html?src=<?php echo $ncodPlyrUrl;?>&url=<?php echo $ncodVdo;?>&image=<?php echo $ncodVdoThumb;?>&key=c4e54deccf4d4ec997a64902e9a30300&type=text%2Fhtml&schema=vimeo\" width=\"940\" height=\"529\" scrolling=\"no\" frameborder=\"0\" allowfullscreen></iframe>",
											"thumbnailUrl": "<?php echo $property_image;?>",
											"width": 940,
											"height": 529
											}] }
										</script>
									</a>
									<?php }
						 echo '</div>
							  </div>';
						echo '<div class="w-col w-col-6 w-col-stack">';
							echo '<div class="featured-post-div featured-prop-div">';
								echo '<h2>'.get_the_title($focusproperty->ID).'</h2>';
								echo '<div>'.get_field("location",$focusproperty->ID).'</div>';
								if( have_rows('property_features',$focusproperty->ID) ):
								    echo '<div class="prop-details">';
									while ( have_rows('property_features',$focusproperty->ID) ) : the_row();
										echo '<div class="prop-desc-row w-row"><div class="w-col w-col-4"><div>'.get_sub_field('column_1',$focusproperty->ID).'</div></div><div class="featured-prop-description prop-desc-col w-col w-col-8"><div>'.get_sub_field('column_2',$focusproperty->ID).'</div></div></div>';
									endwhile;
									echo '</div>';
								endif;
							echo '</div>';
						echo '</div>';
					echo '</div>';
						
						
					}
					
					  echo '</div>';
		echo '</div>';
	echo '</div>';

}
$args = array(
			'posts_per_page' =>-1,
			'post_type' => 'property',
			'post__not_in' => $excludeIds
			);
$wp_query = new WP_Query( $args );
if( $wp_query->have_posts() ){ ?>
	<div class="body-section">
		<div class="post-content-div1180 w-row">
			<!-- <div class="post-left-col w-col w-col-2"></div> -->
			<div class="featured-post-col w-col w-col-12">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<?php 
						$proDtls = get_field('property_features');
						$category = '';
						foreach($proDtls as $key => $value) {
							if(in_array($value['column_1'], array('Category'))) {
								$category = $value['column_2'];
							}
						}
					?>

					<?php if($category == 'Investment Archives'){ ?>
					<div class="post-llink w-inline-block">
						<div class="featured-post-row w-row" data-ix="fade-on-load-3">
							<?php 
							$proVdo = get_field('video_url');
							if($proVdo) {
								if(!empty(get_field("property_video_image"))){
									$bgVal = get_field("property_video_image");
								}else{
									$bgVal = get_template_directory_uri().'/img/597836b3ffd6d400010641c3_blog-thumb-3@2x.jpg';
								}
								$vdoId = (int) substr(parse_url($proVdo, PHP_URL_PATH), 1);
								$ncodVdo = rawurlencode($proVdo);
								$ncodVdoThumb = rawurlencode($bgVal);
								$plyrUrl = 'https://player.vimeo.com/video/'.$vdoId;
								$ncodPlyrUrl = rawurlencode($plyrUrl);
							}else if(has_post_thumbnail()) {
								$bgImgArr = wp_get_attachment_image_src(get_post_thumbnail_id(),'full');
								if(isset($bgImgArr[0]) && $bgImgArr[0]!='') {
									$bgVal = "url('".$bgImgArr[0]."')";
								} else {
									$bgVal = "url('http://via.placeholder.com/485x450')";
								}
							} else {
								$bgVal = "url('http://via.placeholder.com/485x450')";
							}?>
							<div class="w-col w-col-6 w-col-stack">
								<div class="featured-thumb" style="background-image:<?php echo $bgVal;?>">
									<?php if($proVdo) { ?>
										<a class="play-icon-video w-lightbox" href="#">
											<i class="fa fa-play-circle" aria-hidden="true"></i>
											<script class="w-json" type="application/json">{ "items": [{
												"type": "video",
												"url": "<?php echo $proVdo;?>",
												"html": "<iframe class=\"embedly-embed\" src=\"//cdn.embedly.com/widgets/media.html?src=<?php echo $ncodPlyrUrl;?>&url=<?php echo $ncodVdo;?>&image=<?php echo $ncodVdoThumb;?>&key=c4e54deccf4d4ec997a64902e9a30300&type=text%2Fhtml&schema=vimeo\" width=\"940\" height=\"529\" scrolling=\"no\" frameborder=\"0\" allowfullscreen></iframe>",
												"thumbnailUrl": "<?php echo $bgVal;?>",
												"width": 940,
												"height": 529
												}] }
											</script>
										</a>
									<?php } ?>
								</div>
							</div>
							<div class="w-col w-col-6 w-col-stack">
								<div class="featured-post-div">
									<h3><?php echo get_the_title();?></h3>
									<?php $loc =  get_field("location"); 
									if($loc) { ?>
										<div><?php echo $loc;?></div>
									<?php } 
									$proDtls = get_field('property_features');
									if($proDtls) { ?>
										<div class="prop-details">
											<?php foreach($proDtls as $key => $value) { ?>
												<?php 
													//$required_arr = array('Purchase Date', 'Purchase Price', 'Return first 5 yrs', 'Tenants', 'Return in Year 1'); 
													
													if($value['column_1'] == "Information Memorandum"){
														$cscls = "information";
													 }else{
														$cscls = "";
													} 
													
													 if($value['column_1'] == "Category"){
														$cscls1 = "cateinfor";
													 }else{
														$cscls1 = "";
													} 
													
													 if($value['column_1'] == "Video"){
														$cscls2 = "catevideo";
													 }else{
														$cscls2 = "";
													} 
													
													if($value['column_1']!='' && $value['column_2']!=''){
														$cscls3 = "showme";
													}else{
														$cscls3 = "hideme";
													}
													
													?>
												<?php //if(in_array($value['column_1'], $required_arr)) { ?>
													<div class="prop-desc-row w-row <?php echo $cscls; ?> <?php echo $cscls1; ?> <?php echo $cscls2; ?> <?php echo $cscls3; ?>">
														<div class="w-col w-col-4">
															<?php if($value['column_1']) { ?>
																<div><?php echo $value['column_1'];?></div>
															<?php } ?>
														</div>
														<div class="prop-desc-col w-col w-col-8">
															<?php if($value['column_2']) { ?>
																<div><?php echo $value['column_2'];?></div>
															<?php } ?>
														</div>
													</div>
												<?php //} ?>
												<?php if($value['column_1'] == 'Information Memorandum' && !empty($value['column_2'])|| ($value['column_1'] == 'Video' && !empty($value['column_2'])) ) { ?>
													<div class="prop-desc-row info_memo w-row">
														<div class="w-col w-col-12">
															<div><?php echo $value['column_2'];?></div>
														</div>
													</div>
												<?php } ?>
											<?php } ?>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				<?php endwhile;?>
				<a class="load-more-btn back-to-top" href="#"><span class="top-arrow"></span> Back to top</a>
			</div>
		</div>
	</div>
<?php } 
wp_reset_query();?>

<div class="section" style="display:none;">
	<div class="content-div1180 w-clearfix">
		<div class="section-into" data-ix="fade-on-scroll">
			<h2 class="heading-2">News &amp; Views</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus.</p>
		</div>
		<div class="section-content">
			<div class="post-thumb-row">
				<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="#">
					<img class="post-thumb" src="<?php echo get_template_directory_uri().'/img/';?>blog-thumb-12x.jpg">
					<div class="post-thumb-title">Nick Scali reporting "double digit growth" - always great to see one of our tenants getting record</div>
					<div class="post-thumb-date">28 June, 2017</div>
				</a>
				<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="#">
					<img class="post-thumb" src="<?php echo get_template_directory_uri().'/img/';?>blog-thumb-32x.jpg">
					<div class="post-thumb-title">Perth syndicator snaps up Bathurst Supa Centre for $14.7m</div>
					<div class="post-thumb-date">28 June, 2017</div>
				</a>
				<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="#">
					<img class="post-thumb" src="<?php echo get_template_directory_uri().'/img/';?>blog-thumb-22x.jpg">
					<div class="post-thumb-title">Amazon makes major move into bricks and mortar</div>
					<div class="post-thumb-date">28 June, 2017</div>
				</a>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>