<?php /* Template Name: Contact Us Page Template */ get_header(); ?>
<div class="body-section">
	<div class="post-content-div1180 w-row">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
				<?php else: ?>
					<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
