<?php /* Template Name: About Page Template */ get_header(); ?>
<?php
    /*************************Our Team***********************************/ 
    $args = array( 'post_type' => 'team', 'posts_per_page' => -1 );
    $loop = new WP_Query( $args );
	if ( $loop -> have_posts() ):
		echo '<div class="body-section" id="our-team">';
			echo '<div class="intro-div post-content-div1180 w-row" data-ix="fade-on-scroll" >
					<div class="post-left-col w-col w-col-8">
						<h2>'.get_field("team_title").'</h2>
						<p>'.get_field("team_sub_title").'</p>
					</div><div class="post-right-col w-col w-col-4">
					<div></div></div></div>';
			echo '<div class="post-content-div1180 w-row">';
			echo '<div class="post-left-col w-col w-col-2"></div>';
				echo '<div class="featured-post-col w-col w-col-10">';
				echo '<div class="team-row w-row">';
				$t =1;
				$row = 1;
				while ( $loop->have_posts() ) : $loop->the_post();
				    $calCls = $dataix ='';
					$img = get_the_post_thumbnail();
					if($t & 1){
						$calCls = 1;$dataix = 'load-bio1';
					}
					if($t % 2 == 0){
						$calCls = 2;$dataix = 'fade-on-scroll-2';
					}if($t % 3 == 0){
						$calCls = 2;$dataix = 'fade-on-scroll';
					}
					if($t % 4 == 0){
						$calCls = 3;
					}
				 //    if ($t % 3 == 1) {
					// 	echo '<div class="team-row w-row">';
					// }
					if($t == 3 || $t == 6){
						    echo '<div class="w-col w-col-4"><div class="team-col-1"></div></div>';
					}
							echo '<div class="w-col w-col-4"><div class="team-col-'.$calCls.' team-lightbox-click" data-id="'.$t.'">
									<a class="post-thumbnail-parent w-inline-block" data-ix="'.$dataix.'" href="#">'.$img.'<h3 class="team-name">'.get_the_title().'</h3><div class="caption">'.get_field("position").'</div>
									</a>
								  </div>
								  </div>';
						   /***************Team lightbox**********************/
						echo '<div id="team-lightbox-'.$t.'" class="team-lightbox" data-ix="team-lightbox-load" style="opacity: 0; display: none; transition: opacity 200ms ease 0s;">
								<a class="close-lightbox w-inline-block" data-ix="close-team-lightbox" href="#">
									<img src="'.get_template_directory_uri().'/img/lightbox-close-arrow@2x.png" width="36">
								</a>
								<div class="teambio-content-div w-row">
									<div class="teamlightbox-col1 w-col w-col-3">
										<h3 class="team-name">'.get_the_title().'</h3>
										<div class="caption lightbox-caption">'.get_field("position").'</div>
										<div class="social-icons-wrapper">';
										    if( have_rows('social') ):
												while ( have_rows('social') ) : the_row();
												    $icon_link = get_sub_field('icon_link');
													if(!empty($icon_link)){
														echo '<a class="social-footer-btn" href="'.$icon_link.'" target="_blank"><i class="fa '.get_sub_field('icon').'"></i></a>';
													}
											    endwhile;
											endif;
										echo '
										</div>
									</div>
									<div class="w-col w-col-9">
									    <p class="intro-lightbox-p lightbox-p">'.get_the_content().'</p>
									</div>
								</div>
							</div>';
                           /***************Team lightbox**********************/
					// if($t % 2 == 0 && $t % 4 != 0){
					// 	    echo '<div class="w-col w-col-4"><div class="team-col-'.$calCls.'"></div></div>';
					// }
					
					// if($t % 3 == 0){
					// 	echo '</div>';
					// }
				$t++;
				endwhile;
				echo '</div></div>';
			echo '</div>';
		echo '</div>';
	endif;
	wp_reset_query();
/*************************Our Team***********************************/ 
/*************************Why choose Properties & Pathways***********************************/ 
echo '<div class="body-section" id="why-choose">';
		echo '<div class="intro-div post-content-div1180 w-row">
				<div class="post-left-col w-col w-col-8" data-ix="fade-on-scroll" >
					<h2>'.get_field("title").'</h2>
					<p>'.get_field("sub_title").'</p>
				</div>
				<div class="post-right-col w-col w-col-4"><div></div></div>
			  </div>';
		echo '<div class="post-content-div1180 w-row"><div class="post-left-col w-col w-col-2"></div>';	
				echo '<div class="featured-post-col w-col w-col-10">';
					if( have_rows('properties') ):
					    $p = 1;
						while ( have_rows('properties') ) : the_row();
						    $archiveCls = '';if ($p & 1) {$archiveCls= 1;}if($p % 2 == 0){$archiveCls= 2;}
						    if ($p & 1) {
						    echo '<div class="w-row" data-ix="fade-on-scroll" >';
							}   
							    echo '<div class="w-col w-col-6">';
									echo '<div class="archive-col'.$archiveCls.'">';
										echo '<div class="why-choose-div" data-ix="fade-on-scroll" >';
											echo '<h3>'.get_sub_field('name').'</h3>';
											echo '<p>'.get_sub_field('content').'</p>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							if($p % 2 == 0){
							echo '</div>';
							}
							$p++;
						endwhile;
					else :
					endif;
				echo '</div>';
	    echo '</div>';
echo '</div>';
/*************************Why choose Properties & Pathways***********************************/ 
//wp_reset_query();
?>



<!-- Testimonials new -->
<?php
$args = array(
    'post_type' => 'testimonials',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => 3
     );
$query = new WP_Query( $args ); 
$testimonials = $query->posts;


if( !empty($testimonials)){

	echo '<div class="testimonials"><div class="testimoanials slickslider">';

	foreach ($testimonials as $testimonial) {
		$author = get_field("author", $testimonial->ID);
		echo '<div class="slide">';
		echo '<div class="testimonial"><p>'.$testimonial->post_content.'</p></div>';
		echo '<div class="author">'.$author.'</div>';
		echo '</div>';
	}

	echo '</div></div>';

}
?>


<?php get_footer(); ?>