<?php /* Template Name: Home Page Template */ 
get_header(); 
$focusproperties = get_field('please_pick_your_properties','option');
if($focusproperties!=''){
	echo '<div class="body-section featured-prop-section">';
		echo '<div class="post-content-div1180 w-row">';
			echo '<div class="post-left-col w-col w-col-12 w-col-stack"><h2 data-ix="fade-on-load-3" >In Focus</h2></div>';
				//echo '<div class="featured-post-col w-col w-col-10 w-col-stack">';
				echo '<div class="featured-post-col w-col w-col-12 w-col-stack slickslider">';
	
					foreach($focusproperties as $focusproperty){
							
							//echo $focusproperty->ID;
						
							$proVdo = get_field('video_url',$focusproperty->ID);
							if($proVdo) {
								if(!empty(get_field("property_video_image",$focusproperty->ID))){
									$property_image = get_field("property_video_image",$focusproperty->ID);
								}else{
									$property_image = get_template_directory_uri().'/img/597836b3ffd6d400010641c3_blog-thumb-3@2x.jpg';
								}
								$vdoId = (int) substr(parse_url($proVdo, PHP_URL_PATH), 1);
								$ncodVdo = rawurlencode($proVdo);
								$ncodVdoThumb = rawurlencode($property_image);
								$plyrUrl = 'https://player.vimeo.com/video/'.$vdoId;
								$ncodPlyrUrl = rawurlencode($plyrUrl);
							} else {
								$propImgArr = wp_get_attachment_image_src(get_post_thumbnail_id($focusproperty->ID),'full');
								if(isset($propImgArr[0]) && $propImgArr[0]!='') {
									$property_image = $propImgArr[0];
								} else {
									$property_image = get_template_directory_uri().'/img/597836b3ffd6d400010641c3_blog-thumb-3@2x.jpg';
								}
							}
					
					echo '<div class="featured-post-row featured-prop-row w-row" data-ix="fade-on-load-3">';
						echo '<div class="featured-post-row-wrapper">';
							echo '<div class="column w-col w-col-6 w-col-stack">
								<div class="featured-prop-thumb featured-thumb" style="background-image: url(\''.$property_image.'\')">';
									if($proVdo) {?>
									<a class="play-icon-video w-lightbox" href="#"><i class="fa fa-play-circle" aria-hidden="true"></i>
									<script class="w-json" type="application/json" data-src="<?php echo $proVdo;?>" data-img="<?php echo $property_image;?>">{ "items": [{
										"type": "video",
										"url": "<?php echo $proVdo;?>",
										"html": "<iframe class=\"embedly-embed\" src=\"//cdn.embedly.com/widgets/media.html?src=<?php echo $ncodPlyrUrl;?>&url=<?php echo $ncodVdo;?>&image=<?php echo $ncodVdoThumb;?>&key=c4e54deccf4d4ec997a64902e9a30300&type=text/html&schema=vimeo\" width=\"940\" height=\"529\" scrolling=\"no\" frameborder=\"0\" allowfullscreen></iframe>",
										"thumbnailUrl": "<?php echo $property_image;?>",
										"width": 940,
										"height": 529
										}] }
									</script>
									</a>
									<?php }
						 echo '</div>
							  </div>';
						echo '<div class="w-col w-col-6 w-col-stack">';
							echo '<div class="featured-post-div featured-prop-div">';

								if( have_rows('property_features',$focusproperty->ID) ):
									$required_arr = array('Category');
									while ( have_rows('property_features',$focusproperty->ID) ) : the_row();
										$detail_name = get_sub_field('column_1',$focusproperty->ID);
										$detail_value = get_sub_field('column_2',$focusproperty->ID);
										if(in_array($detail_name, $required_arr)) {
											echo '<div>'.$detail_value.'</div>';
										}
									endwhile;
								endif;

								echo '<h2>'.get_the_title($focusproperty->ID).'</h2>';
								echo '<div>'.get_field("location",$focusproperty->ID).'</div>';
								if( have_rows('property_features',$focusproperty->ID) ):
								    echo '<div class="prop-details">';
								    //$required_arr = array('Purchase Date', 'Purchase Price', 'Return first 5 yrs', 'Tenants', 'Return in Year 1');
									
									while ( have_rows('property_features',$focusproperty->ID) ) : the_row();
										$detail_name = get_sub_field('column_1',$focusproperty->ID);
										$detail_value = get_sub_field('column_2',$focusproperty->ID);
										
										 if($detail_name == "Information Memorandum"){
														$cscls = "information";
													 }else{
														$cscls = "";
													} 
													
													 if($detail_name == "Category"){
														$cscls1 = "cateinfor";
													 }else{
														$cscls1 = "";
													} 
													
													 if($detail_name == "Video"){
														$cscls2 = "catevideo";
													 }else{
														$cscls2 = "";
													} 
													
													if($detail_name!='' && $detail_value!=''){
														$cscls3 = "showme";
													}else{
														$cscls3 = "hideme";
													} 
													
										/* if(in_array($detail_name, $required_arr)) { */
											echo '<div class="prop-desc-row w-row '.$cscls.' '.$cscls1.' '.$cscls2.' '.$cscls3.'"><div class="w-col w-col-4"><div>'.$detail_name.'</div></div><div class="featured-prop-description prop-desc-col w-col w-col-8"><div>'.$detail_value.'</div></div></div>';
										/* } */
										if(($detail_name == 'Information Memorandum' && !empty($detail_value)) || ($detail_name == 'Video' && !empty($detail_value))) {
											echo '<div class="prop-desc-row info_memo w-row"><div class="w-col w-col-12"><div>'.$detail_value.'</div></div></div>';
										}
									endwhile;
									echo '</div>';
								endif;
							echo '</div>';
						echo '</div>';
					echo '</div>';
					echo '</div>';
						
						
					}
					
					  echo '</div>';
		// echo '</div>';
	echo '</div>';

}
/*************************Property Listing***********************************/ 
while ( have_rows( 'home_page_builder' ) ) : the_row(); 
	get_template_part( 'partials/block', get_row_layout() ); 
endwhile;
?>
<div class="section" style="display:none;">
	<div class="content-div1180 w-clearfix">
		<div class="section-into" data-ix="fade-on-scroll">
			<h2 class="heading-2">News &amp; Views</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus.</p>
		</div>
		<div class="section-content">
			<div class="post-thumb-row">
				<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="#">
					<img class="post-thumb" src="<?php echo get_template_directory_uri().'/img/';?>blog-thumb-12x.jpg">
					<div class="post-thumb-title">Nick Scali reporting "double digit growth" - always great to see one of our tenants getting record</div>
					<div class="post-thumb-date">28 June, 2017</div>
				</a>
				<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="#">
					<img class="post-thumb" src="<?php echo get_template_directory_uri().'/img/';?>blog-thumb-32x.jpg">
					<div class="post-thumb-title">Perth syndicator snaps up Bathurst Supa Centre for $14.7m</div>
					<div class="post-thumb-date">28 June, 2017</div>
				</a>
				<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="#">
					<img class="post-thumb" src="<?php echo get_template_directory_uri().'/img/';?>blog-thumb-22x.jpg">
					<div class="post-thumb-title">Amazon makes major move into bricks and mortar</div>
					<div class="post-thumb-date">28 June, 2017</div>
				</a>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>