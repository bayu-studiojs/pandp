<?php /* Template Name: Our Services Page Template */ get_header(); ?>
<?php
/*************************Services Listing***********************************/
$heading = get_field('services_heading');
$content = get_field('services_content');
if( have_rows('service_list') ||  !empty($heading) ||  !empty($heading) ){
echo '<div class="body-section">';
	echo '<div class="intro-div post-content-div1180 w-row"><div class="post-left-col w-col w-col-8" data-ix="fade-on-scroll" ><h2>'.$heading.'</h2><p>'.$content.'</p></div><div class="post-right-col w-col w-col-4"><div></div></div></div>';
	if( have_rows('service_list')){
		echo '<div class="post-content-div1180 w-row">';
			echo '<div class="post-left-col w-col w-col-2"></div>';
			echo '<div class="featured-post-col w-col w-col-10">';
			    $s = 1;
				while ( have_rows('service_list') ) : the_row();
				    $icon = get_sub_field('icon');$title = get_sub_field('title');$content = get_sub_field('content');
					if($s & 1){$arcCol=1;}else{$arcCol=2;}
				    if($s & 1){
						echo '<div class="w-row" data-ix="fade-on-scroll" >';
					}
							echo '<div class="w-col w-col-6"><div class="archive-col'.$arcCol.'"><div class="services-div" data-ix="fade-on-scroll" >';
								if($icon){
									echo '<img class="services-icon" src="'.$icon.'">';
								}
								echo '<div>';
								if($title){
									echo '<h3 class="services-h3">'.$title.'</h3>';
								}
								if($content){
									echo '<p>'.$content.'</p>';
								}
								echo '</div>';
							echo '</div></div></div>';
					if($s % 2 == 0){
						echo '</div>';
					}
					$s++;
				endwhile;
			echo '</div>';
		echo '</div>';
	}
echo '</div>';
}
/*************************Services Listing***********************************/
//wp_reset_query();
?>

<?php get_footer(); ?>