<?php get_header(); 
$args = array(
			'posts_per_page' =>2,
			'post_type' => 'post',
			);
$wp_query = new WP_Query( $args );
if( $wp_query->have_posts() ){ ?>
	<div class="body-section blog">
		<div class="post-content-div1180 w-row">
			<div class="post-left-col w-col w-col-2">
				<h2>Featured Posts</h2>
			</div>
			<div class="featured-post-col w-col w-col-10">
				<?php 
				$excludeIds = array();
				while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
					$excludeIds[] = get_the_ID();
				?>
					<div class="post-llink-cntnr">
						<a class="post-llink w-inline-block"  href="<?php echo get_permalink();?>">&nbsp;</a>
						<div class="featured-post-row w-row" data-ix="fade-on-load-3">
							<div class="w-col w-col-6">
								<?php if(has_post_thumbnail()) {
									$bgImgArr = wp_get_attachment_image_src(get_post_thumbnail_id(),'featured-post-thumb');
									if(isset($bgImgArr[0]) && $bgImgArr[0]!='') {
										$bgVal = "url('".$bgImgArr[0]."')";
									} else {
										$bgVal = "url('http://via.placeholder.com/632x402')";
									}
								} else {
									$bgVal = "url('http://via.placeholder.com/632x402')";
								}?>
								<div class="featured-thumb" style="background-image:<?php echo $bgVal;?>"></div>
							</div>
							<div class="w-col w-col-6">
								<div class="featured-post-div">
									<div class="post-date"><?php echo get_the_date();?></div>
									<h2><?php echo get_the_title();?></h2>
									<?php $intro = get_field('intro');
									if(!$intro) {
										$intro = get_the_excerpt();
									} ?>
									<div class="paragraph"><?php echo limit_words($intro,15).'...';?></div>
									<div class="read-more">Read More<span class="read-more-arrow"><i class="fa fa-angle-right"></i></span></div>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile;?>
			</div>
		</div>
	</div>
<?php }
wp_reset_query(); 
$args = array(
			'posts_per_page' =>8,
			'post_type' => 'post',
			'post__not_in' => $excludeIds
			);
$wp_query = new WP_Query( $args );?>
<div class="body-section">
	<div class="post-content-div1180 w-row">
		<?php if( $wp_query->have_posts() ){
			$totalPost = $wp_query->found_posts;?>
			<div class="post-left-col w-col w-col-2 w-col-small-small-stack">
				<h2>Archive</h2>
			</div>
			<div class="post-middle-col w-col w-col-7 w-col-small-small-stack">
				<div class="w-row" data-ix="fade-on-scroll">
					<?php 
					$i = 0;
					while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<div class="w-col w-col-6">
						<div class="archive-col<?php echo ($i%2==0) ? 1 : 2;?> archive-post">
							<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="<?php echo get_permalink();?>">
								<?php if(has_post_thumbnail()) {
									$imgArr = wp_get_attachment_image_src(get_post_thumbnail_id(),'archive-post-thumb');
									if(isset($imgArr[0]) && $imgArr[0]!='') {
										$imgSrc = $imgArr[0];
									} else {
										$imgSrc = 'http://via.placeholder.com/622x402';
									}
								} else {
									$imgSrc = 'http://via.placeholder.com/622x402';
								}?>
								<img class="post-thumb" src="<?php echo $imgSrc; ?>">
								<div class="post-thumb-title"><?php echo get_the_title();?></div>
								<div class="post-thumb-date"><?php echo get_the_date();?></div>
							</a>
						</div>
					</div>
					<?php 
					if($i%2==1) {
						echo '</div><div class="w-row" data-ix="fade-on-scroll">';
					}
					$i++;
					endwhile;?>
				</div>
				<?php if(isset($totalPost) && $totalPost>8) { ?>
					<a class="load-more-btn" href="javascript:void(0);" onclick="getMorePost();">Load More</a>
					<input type="hidden" id="total-post" value="<?php echo $totalPost;?>" />
					<?php foreach($excludeIds as $key => $value) { ?>
						<input type="hidden" class="exclude-ids" name="exclude-ids[]" value="<?php echo $value;?>" />
					<?php } 
				} ?>
				<input type="hidden" id="paged" value="1" />
			</div>
		<?php } 
		wp_reset_query();
		$catList = get_categories();
		if($catList) { ?>
			<div class="post-right-col w-col w-col-3 w-col-small-small-stack">
				<div>
					<div class="w-row">
						<div class="w-col w-col-3"></div>
						<div class="w-col w-col-9">
							<div class="post-sidebar-heading">Category</div>
							<ul class="related-posts">
								<?php foreach($catList as $key => $value) { ?>
									<li class="related-post-listitem">
										<a href="<?php echo get_category_link($value->term_id);?>"><?php echo $value->name;?></a>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?php get_footer(); ?>
