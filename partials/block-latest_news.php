<?php /*************************Latest News Listing***********************************/ 
$args = array(
	'orderby' => 'date',
	'order' => 'DESC',
	'posts_per_page' =>3,
	'post_type' => 'post'
);
$wp_query = new WP_Query( $args );
if( $wp_query->have_posts() ){
	echo '<div class="body-section latest-news-sec"><div class="post-content-div1180 w-row">';
		echo '<div class="post-left-col w-col w-col-12 w-col-stack"><h2 data-ix="fade-on-scroll" style="opacity: 1; transform: translateX(0px) translateY(0px) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">Latest News</h2></div>';
		echo '<div class="w-col w-col-12 w-col-stack home-latest-news">';
		    echo '<div class="team-row w-row">';
			    $c = 1;
				while ( $wp_query->have_posts() ) : $wp_query->the_post();
				    $Colval='';
				    $Cls= $c % 3;if($Cls == 1 || $Cls == 2){$Colval = $Cls;}else{$Colval = 3;}
					echo '<div class="w-col w-col-4">';
						if(has_post_thumbnail()) {
							$bgImgArr = wp_get_attachment_image_src(get_post_thumbnail_id(),'featured-post-thumb');
							if(isset($bgImgArr[0]) && $bgImgArr[0]!='') {
								$bgVal = $bgImgArr[0];
							} else {
								$bgVal = 'http://via.placeholder.com/622x402';
							}
						} else {
							$bgVal = 'http://via.placeholder.com/622x402';
						}
						echo '<div class="team-col-'.$Colval.'">';
							echo '<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="'.get_the_permalink().'" >';
								echo '<img class="post-thumb" src="'.$bgVal.'">';
								echo '<h3>'.get_the_title().'</h3>';
								echo '<div class="caption">'.get_the_date('d M, Y').'</div>';
							echo '</a>';
						echo '</div>';
					echo '</div>';
					$c++;
				endwhile;
			echo '</div>';
		echo '</div>';
	echo '</div></div>';
}
wp_reset_query();
/*************************Latest News Listing***********************************/ 
?>