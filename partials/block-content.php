<?php $title = get_sub_field('title');
$desc = get_sub_field('description');
?>
<div class="lead-cmrcl-sec">
	<div class="post-content-div1180 w-row">
		<?php if($title) { ?>
			<h1><?php echo $title;?></h1>
		<?php } 
		if($desc) { 
			echo $desc;
		} ?>
	</div>
</div>