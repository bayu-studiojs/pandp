<?php 
$secTitle = get_sub_field('frmtitle');
//$secTitle = 'Book Your Free Consultation';
if(is_page_template('pagetemplates/template-landing.php') || is_page_template('pagetemplates/template-landing-v2.php')) {
	$frmId = 4;
} else {
	$frmId = get_sub_field('form');
}
$tel = get_field('contact_telephone','option');
$address = get_field('contact_address','option');

?>
<div class="hme-cntnct-sec" id="hme-cntnct-sec">
	<?php if($secTitle) { ?>
		<div class="post-content-div1180">
			<h2><?php echo $secTitle;?></h2>
		</div>
	<?php } ?>
	<div class="post-content-div1180 w-row">
		<div class="featured-post-col w-col w-col-3">
			<?php if($tel) { ?>
			<h4>Telephone</h4>
				<h3><a href="tel:<?php echo $tel;?>" style="color:#e4802d;"><?php echo $tel;?></a></h3>
			<?php } 
			if($address) { 
				foreach($address as $key => $value) { 
					if($value['title']) { ?>
						<h4><?php echo $value['title'];?></h4>
					<?php } 
					if($value['address']) { ?>
						<h3><?php echo $value['address'];?></h3>
					<?php }
				} 
			} ?>
		</div>
		<div class="w-col w-col-2">&nbsp;</div>
		<?php if($frmId) { ?>
			<div class="contact-form-col w-col w-col-7 ajaxform1">
				<div class="w-form"><?php echo do_shortcode('[gravityform id="'.$frmId.'" title="false" description="false" ajax="true"]');?></div>
			</div>
		<?php } ?>
	</div>
</div>