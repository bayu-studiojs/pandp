<?php $content = get_sub_field('content');
if($content) { ?>
	<div class="full-width-column-sec">
		<div class="post-content-div1180 w-row">
			<div class="w-col w-col-12">
				<?php echo $content;?>
			</div>
		</div>
	</div>
<?php } ?>