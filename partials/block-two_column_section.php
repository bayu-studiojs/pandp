<!-- Testimonials new -->
<?php
$testimonials = get_sub_field('testimonial');
$tstmnlTitle = get_sub_field('title');
if( !empty($testimonials)){
	echo '<div class="testimonials">
		<div class="post-content-div1180 w-row">
			<div class="w-col w-col-6">';
				if($tstmnlTitle) {
					echo '<h2>'.$tstmnlTitle.'</h2>';
				}
				echo '<div class="testimoanials slickslider">';
				foreach ($testimonials as $testimonial) {
					$author = get_field("author", $testimonial->ID);
					$testContent = (str_word_count($testimonial->post_content)>30) ? limit_words($testimonial->post_content,30)."..." : $testimonial->post_content;
					echo '<div class="slide">';
						echo '<div class="testimonial"><p>'.$testContent.'</p></div>';
						echo '<div class="author">'.$author.'</div>';
					echo '</div>';
				}
				echo '</div>
			</div>';
}
$vdoUrl = get_sub_field('video_url');
$vdoId = (int) substr(parse_url($vdoUrl, PHP_URL_PATH), 1);
$vdoTitle = get_sub_field('title_second_column');
			echo '<div class="w-col w-col-6">';
				if($tstmnlTitle) {
					echo '<h2>'.$vdoTitle.'</h2>';
				}
				if($vdoUrl) { 
				echo '<div class="home-vdo-cntnr">
					<iframe class="embedly-embed" src="//cdn.embedly.com/widgets/media.html?src=https://player.vimeo.com/video/'.$vdoId.'&url='.$vdoUrl.'&image=http://staging.propertiesandpathways.com/wp-content/uploads/2017/08/home-video-thumb.jpg&key=c4e54deccf4d4ec997a64902e9a30300&type=text/html&schema=vimeo" width="530" height="300" scrolling="no" frameborder="0" allowfullscreen></iframe>
				</div>';
				} 
			echo '</div>
		</div>
		
	</div>';?>