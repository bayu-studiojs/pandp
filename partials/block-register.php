<div class="footercta">
<div class="w-container">
  <h2 class="h2centred h2orange"><?php echo get_field('ctasec_rheading', 'option'); ?></h2>
  <div id="footerregistration"><?php echo get_field('ctasec_rdescription', 'option'); ?></div>
  <div class="footerctadesktop">
	<h3><?php echo get_field('book_for_free_rheading', 'option'); ?></h3><a href="#contact" class="ctasubmitbutton footer w-button">Register Now</a></div>
  <div class="footerctaparent">
	<div class="ctadiv footerregistration">
	  <h2 class="ctah"><?php echo get_field('book_for_free_rheading', 'option'); ?></h2>
	  <?php echo do_shortcode(get_field('book_free_rform', 'option'));?>
	</div>
  </div>
</div>
</div>