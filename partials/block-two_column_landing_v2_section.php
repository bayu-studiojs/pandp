<?php $lftCntnt = get_sub_field('left_content');
$rghtImg = get_sub_field('image');
$imgCptn = get_sub_field('image_caption');
$imgMeta = get_sub_field('image_meta');
$imgCmpny = get_sub_field('image_company');?>
<div class="two-column-sec">
	<div class="post-content-div1180 w-row">
		<?php if($lftCntnt) { ?>
		<div class="w-col w-col-7">
			<?php echo $lftCntnt;?>
		</div>
		<?php } ?>
		<div class="w-col w-col-4">
			<div class="img-cntnr">
				<?php if($rghtImg) { ?>
					<img src="<?php echo $rghtImg;?>" />
				<?php } ?>
				<div class="img-cptn-cntnr">
					<?php if($imgCptn) { ?>
						<h3><?php echo $imgCptn;?></h3>
					<?php } if($imgMeta) {  ?>
						<p><?php echo $imgMeta;?></p>
					<?php } if($imgCmpny) { ?>
						<span><?php echo $imgCmpny;?></span>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>