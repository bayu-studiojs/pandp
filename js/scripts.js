(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// DOM ready, take it away
		
	});
	
})(jQuery, this);
jQuery(document).ready(function() {
	jQuery('.down-arrow-new').click(function() {
		var parEle = jQuery(this).closest(".home-hero-parent");
		var nxtEle = jQuery(parEle).nextAll('div:visible').addClass('scrollto');
		jQuery('html, body').animate({
			scrollTop: jQuery('.scrollto').offset().top
		}, 2000);
		jQuery('.scrollto').removeClass('scrollto');
	});
	jQuery('.team-lightbox-click').click(function() {
		var dataid = jQuery(this).attr("data-id");
		//jQuery('#team-lightbox-'+dataid).css({'display' : 'block','opacity' : '1','transition' : 'opacity 200 ease 0'});
		//jQuery('#team-lightbox-'+dataid).fadeIn();
		  jQuery('#team-lightbox-'+dataid).fadeIn( 100, function() {
			jQuery(this).css('opacity','1');
		  });
	});
	jQuery('.faq-question').click(function() {
		var classVal = jQuery(this).attr('data-div');
			var topVal = (jQuery('.'+classVal).offset().top)-85;
			jQuery('html, body').animate({
		   scrollTop: topVal
		  }, 2000);
	});
	jQuery(".back-to-top").click(function () {jQuery("html, body").animate({scrollTop: 0}, 1000);});
	jQuery('.gform_wrapper input[type=text],.gform_wrapper input[type=email],.gform_wrapper input[type=tel],.gform_wrapper input[type=password]').addClass('w-input text-field');
	jQuery('.ctadiv .gform_wrapper input[type=submit]').addClass('ctasubmitbutton w-button');
	//jQuery('.w-lightbox').first().trigger('tap');
	/*jQuery('.faq-question').click(function() {
	play-icon-video
	}*/


	jQuery('.slickslider').slick({
	  dots: true,
	  infinite: true,
	  speed: 600,
	  fade: true,
	  autoplay: true,
	  autoplaySpeed: 5000,
	  cssEase: 'linear'
	});
	
	property_height();
	jQuery('a.lightbox-nav-btn').click(function() {
		jQuery('.nav-lightbox').css({
			'opacity' : '1',
			'visibility' : 'visible'
		});
	});
	jQuery('a.close-nav-btn').click(function() {
		jQuery('.nav-lightbox').css({
			'opacity' : '0',
			'visibility' : 'hidden'
			
		});
	});
});

function getMorePost() {
	var paged = parseInt(jQuery('#paged').val());
	var excludeIds = new Array();
	jQuery('.exclude-ids').each(function() {
		excludeIds.push(jQuery(this).val()); 
	});
	jQuery.ajax({
		type: 'POST', 
		url: siteUrl+'/wp-admin/admin-ajax.php',  
		dataType:"json",
		data: { action: 'getMorePost',paged:paged,excludeIds:excludeIds },
		success: function(data){
			if(typeof(data.content) != "undefined" && data.content !== null){ 
				jQuery(data.content).insertBefore( ".load-more-btn" );
				jQuery('#paged').val(paged+1);
			} else {
				jQuery('.post-middle-col').html('No Post Found');
			}
			if(typeof(data.totalpost) != "undefined" && data.totalpost !== null){ 
				var totalPost = parseInt(data.totalpost);
				var displayedPost = parseInt(jQuery('.archive-post').length);
				if(totalPost<=displayedPost) {
					jQuery('.load-more-btn').hide();
				} else {
					jQuery('.load-more-btn').show();
				}
			}
		}
	});
}

function property_height() {
	jQuery(function() {
	    jQuery('.featured-post-row').find('.featured-post-row.featured-prop-row .w-col-6').css('height', jQuery('.featured-post-row').innerHeight());
	});
	//jQuery('.slick-track').find('.child').css('height', $('.parent').innerHeight());
}

/*var Webflow = Webflow || [];
Webflow.push(function() {
  /*var json = $('.w-json[data-src][data-img]');
  json.each(function() {
    var youtube = $(this).attr('data-src').match(/[^=]+$/);
    var src = encodeURIComponent($(this).attr('data-src'));
    var img = encodeURIComponent($(this).attr('data-img'));
    if(typeof youtube === 'undefined') return; // not valid youtube video
    $(this).html($(this).html().replace('{YOUTUBE}', youtube[0]).replace('{SRC}', src).replace('{IMG}', img));
  });
  //jQuery('.w-lightbox').webflowLightBox();
});*/