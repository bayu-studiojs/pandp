<?php get_header(); 
if (have_posts()): while (have_posts()) : the_post(); ?>
	<div class="body-section">
		<div class="post-content-div1180 w-row">
			<div class="post-left-col w-col w-col-2 w-col-small-small-stack">
				<div class="post-sidebar-heading">Share this post</div>
				<!-- Go to www.addthis.com/dashboard to customize your tools -->
				<div class="addthis_inline_share_toolbox"></div>
				<!-- <div class="social-icons-wrapper"><a class="social-footer-btn" href="#"></a><a class="social-footer-btn" href="#"></a></div> -->
			</div>
			<div class="post-middle-col w-col w-col-7 w-col-small-small-stack">
				<?php $intro = get_field('intro');
				if($intro) { ?>
					<div class="intro large"><?php echo $intro;?></div>
				<?php } 
				if ( has_post_thumbnail()) { // Check if Thumbnail exists ?>
					<img class="featured-image" src="<?php echo get_the_post_thumbnail_url(); ?>">
				<?php } ?>
				<div class="large"><?php the_content();?></div>
			
			</div>
			<?php $args = array(
				'post_type' => 'post',
				'posts_per_page' =>3
				);
			$excludeIds = array(get_the_ID());
			if(isset($excludeIds) && count($excludeIds)>0) {
				$args['post__not_in'] = $excludeIds;
			}
			$category_detail=get_the_category();
			if($category_detail) {
				$catId = $category_detail[0]->term_id;
				if($catId) {
					$args['cat'] = $catId;
				}
			}
			$wp_query = new WP_Query( $args );
			if( $wp_query->have_posts() ){ ?>
				<div class="post-right-col w-col w-col-3 w-col-small-small-stack">
					<div>
						<div class="w-row">
							<div class="w-col w-col-3"></div>
							<div class="w-col w-col-9">
								<div class="post-sidebar-heading">You may also like</div>
								<ul class="related-posts">
									<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
										<li class="related-post-listitem">
											<a href="<?php echo get_permalink();?>"><?php echo get_the_title();?></a>
										</li>
									<?php endwhile; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			<?php } 
			wp_reset_query();?>
		</div>
	</div>
<?php endwhile; 
endif; ?>
<?php get_footer(); ?>