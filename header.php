<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" rel="shortcut icon">
        

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>"> 
 

        <meta property='og:title' content="<?php echo bloginfo('name')." - ".get_the_title(); ?>"/>
        <meta property='og:image' content="<?php echo get_template_directory_uri(); ?>/img/pp-logo.png"/>
        <meta property='og:description' content="<?php bloginfo('description'); ?>"/>
        <meta property='og:url' content="<?php echo get_permalink(); ?>" /> 
        
        <?php // echo bloginfo('description'); ?>
        

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
		<script src="https://use.typekit.net/npv8trd.js" type="text/javascript"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		<script type="text/javascript">
			var siteUrl = '<?php echo get_site_url() ?>';
			var tempUrl = '<?php echo get_template_directory_uri(); ?>';
		</script>
		<script type="text/javascript">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-103831329-1', 'auto');
		ga('send', 'pageview');
		</script>
	</head>
	<?php if(is_front_page()) { 
		$bdyClsVal = 'body-2';
	} else {
		$bdyClsVal = 'body';
	} ?>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59d5dea8897c7f4c"></script>
 
		
	<body <?php body_class($bdyClsVal); ?>>

<?php 
$hdrMenu = wp_get_nav_menu_items("Header Menu");
//$hdrMenu = wp_get_nav_menu_items("Header Contact Menu");
$hdrLogo = get_field('header_logo','option');
if(!$hdrLogo) {
	$hdrLogo = get_template_directory_uri().'/img/pp-nav-logo-white2x.png';
}
$mblHdrLogo = get_field('mobile_header_logo','option');
if(!$mblHdrLogo) {
	$mblHdrLogo = get_template_directory_uri().'/img/responsive-nav-logo2x.png';
}
$scrlHdrLogo = get_field('scroll_nav_logo','option');
if(!$scrlHdrLogo) {
	$scrlHdrLogo = get_template_directory_uri().'/img/responsive-nav-logo2x.png';
} ?>		
			<?php if(!is_front_page()) { ?>
				<div class="main-wrapper">
					<?php if(is_single()) { 
						$imgClsVal = 'body-bg-copy';
					}else if(is_page_template('pagetemplates/template-about-us.php') || is_page_template('pagetemplates/template-our-services.php')){
						$imgClsVal = 'about';
					} else {
						$imgClsVal = 'body-bg';
					} ?>
					<img class="<?php echo $imgClsVal;?> hero-bg" src="<?php echo get_template_directory_uri();?>/img/body-bg-v22x.png" width="448">
			<?php }
			if($hdrMenu) { ?>
				<!-- header -->
				<div class="header">
					
					<div class="responsive-nav-wrapper w-hidden-main">
						<div class="nav-lightbox" data-ix="lightbox-nav-load">
							<a class="lightbox-nav-link" href="<?php echo get_site_url();?>">Home</a>
							<?php foreach($hdrMenu as $key => $value) { ?>
								<a class="lightbox-nav-link <?php if($value->menu_item_parent!=0) { ?>child-nav-itm<?php } ?>" href="<?php echo $value->url;?>"><?php echo $value->title;?></a>
							<?php } ?>
							<a class="close-nav-btn w-inline-block" data-ix="close-nav" href="#">
								<img src="<?php echo get_template_directory_uri();?>/img/close-nav.svg">
							</a>
						</div>
						<div class="responsive-nav">
							<a class="w-inline-block" href="#">
								<img src="<?php echo $mblHdrLogo;?>" width="150">
							</a>
							<a class="lightbox-nav-btn w-inline-block" data-ix="show-nav-lightbox" href="#">
								<img src="<?php echo get_template_directory_uri();?>/img/hamburger-icon.svg">
							</a>
						</div>
					</div>
					<div class="scroll-nav w-hidden-medium w-hidden-small w-hidden-tiny" data-ix="scroll-nav-display-none">
						<a class="w-inline-block" href="<?php echo get_site_url();?>">
							<img src="<?php echo $scrlHdrLogo;?>" width="57">
						</a>
						<div class="nav-links-parent">
							<?php echo html5blank_nav(); ?>
						</div>
					</div>
					<div class="desktop-nav w-hidden-medium w-hidden-small w-hidden-tiny" data-ix="display-nav">
						<a class="w-inline-block" href="<?php echo get_site_url();?>"><img src="<?php echo $hdrLogo;?>" width="193"></a>
						<div class="nav-links-parent">
							<?php echo html5blank_nav(); ?>
						</div>
					</div>
					
					 <?php if(is_page_template('pagetemplates/template-service-child-landing.php')){ 
						$frmHdng = get_field('form_heading');
						$formId = get_field('form_item'); ?>
					  <div class="ctawrapper" id="contact">
						  <div class="ctacontainer">
							<div class="ctadiv">
							  <?php if($frmHdng) { ?>
								<h2 class="ctah"><?php echo $frmHdng; ?></h2>
							  <?php } 
							  if($formId) { ?>
							  <div class="w-form">
								<?php echo do_shortcode('[gravityform id="'.$formId.'" title="false" description="false"]');?>
							  </div>
							  <?php } ?>
							</div>
						  </div>
						</div>
					  <?php } ?>
				</div>
				<!-- /header -->
			<?php }
			if(is_front_page()) { 
				$bnrTtl = get_field('banner_title');
				if(!$bnrTtl) {
					$bnrTtl = get_the_title();
				}
				$bnrImg = get_field('banner_image');
				$bnrLnkType = get_field('banner_link_type');
				if($bnrLnkType=='video') {
					$bnrVdo = get_field('banner_video_url');
					$bnrVdoThmb = get_field('banner_video_thumb');
					if(!$bnrVdoThmb) {
						$bnrVdoThmb = 'https://i.vimeocdn.com/video/579078846_1280.jpg';
					}
					if($bnrVdo) {
						$vdoId = (int) substr(parse_url($bnrVdo, PHP_URL_PATH), 1);
						$ncodVdo = rawurlencode($bnrVdo);
						$ncodVdoThumb = rawurlencode($bnrVdoThmb);
						$plyrUrl = 'https://player.vimeo.com/video/'.$vdoId;
						$ncodPlyrUrl = rawurlencode($plyrUrl);
					}
				} else if($bnrLnkType=='button') {
					$bnrBtnTxt = get_field('banner_button_text');
					$bnrBtnLnk = get_field('banner_button_link');
				}?>
				<div class="home-hero-parent" <?php if($bnrImg) { ?>style="background-image:url('<?php echo $bnrImg;?>');"<?php } ?>>
					<div class="hero-content-wrapper">
						<div class="content-div1180">
							<div class="hero-content-parent">
								<h1 data-ix="fade-on-load"><?php echo $bnrTtl;?></h1>
								<div class="hero-paragraph">
									<p class="" data-ix="fade-on-load-2" ><?php echo get_field('banner_sub_title');?></p>
								</div>
								<div class="hero-btn-parent" data-ix="fade-on-load-2">
									<?php if($bnrLnkType=='video') { ?>
										<a class="home-video-link w-clearfix w-inline-block w-lightbox" href="#">
											<img class="play-icon" src="<?php echo get_template_directory_uri();?>/img/play-icon2x.png" width="38">
											<div>Watch Our Video</div>
											<script class="w-json" type="application/json">{ "items": [{
												"type": "video",
												"url": "<?php echo $bnrVdo;?>",
												"html": "<iframe class=\"embedly-embed\" src=\"//cdn.embedly.com/widgets/media.html?src=<?php echo $ncodPlyrUrl;?>&url=<?php echo $ncodVdo;?>&image=<?php echo $ncodVdoThumb;?>&key=c4e54deccf4d4ec997a64902e9a30300&type=text%2Fhtml&schema=vimeo\" width=\"940\" height=\"529\" scrolling=\"no\" frameborder=\"0\" allowfullscreen></iframe>",
												"thumbnailUrl": "<?php echo $bnrVdoThmb;?>",
												"width": 940,
												"height": 529
												}] }
											</script>
										</a>
									<?php } else if($bnrLnkType=='button') { ?>
										<a href="<?php echo $bnrBtnLnk;?>" class="home-bnr-btn"><?php echo $bnrBtnTxt;?></a>
									<?php } ?>
								</div> 
								<?php /* Digital Creative - Update */
								/* $focusproperties = get_field('please_pick_your_properties','option');
								if($focusproperties) { 
									$focusproperties = $focusproperties[0]; ?>
									<a href="<?php echo get_permalink(12);?>">
										<div class="prop-focus-hero-parent" data-ix="fade-on-load-3">
											<div class="prop-focus-heading">Property In Focus</div>
											<div class="prop-focus"><span class="prop-focus-name"><?php echo $focusproperties->post_title;?></span> <?php echo get_field('location',$focusproperties->ID);?></div>
										</div>
									</a>
								<?php } */ ?>
								<a class="down-arrow-new w-inline-block" data-ix="fade-on-load-4" href="#">
									<img src="<?php echo get_template_directory_uri();?>/img/down-arrow-white2x.png" width="36">
								</a>
							</div>
						</div>
					</div>
					<img class="hero-bg" src="<?php echo get_template_directory_uri();?>/img/body-bg-v22x.png" width="448">
				</div>
			<?php }else if(is_page_template('pagetemplates/template-about-us.php')){
				$bnrTtl = get_field('banner_title');
				if(!$bnrTtl) {
					$bnrTtl = get_the_title();
				}
				$bnrSubTtl = get_field('banner_sub_title');
				$bnrImg = get_field('banner_image');
				?>
				<div class="body-header brochure" <?php if($bnrImg) { ?>style="background-image:url('<?php echo $bnrImg;?>');"<?php } ?>>
					<div class="content-div1180">
						<div class="header-content-dv">
							<div class="page-title"><?php echo get_the_title();?></div>
							<h1 data-ix="fade-on-load"><?php echo $bnrTtl;?></h1>
							<div class="hero-paragraph">
								<p  class="" data-ix="fade-on-load-2"><?php echo $bnrSubTtl;?></p>
							</div>
							<div class="anchor-btns"><a class="anchor-btn read-more" href="#our-team">Our Team<span class="read-more-arrow"></span></a><a class="read-more" href="#why-choose">Why Choose Properties &amp; Pathways <span class="read-more-arrow"></span></a></div>
							<a class="down-arrow-new w-inline-block" data-ix="fade-on-load-4" href="#">
								<img src="<?php echo get_template_directory_uri();?>/img/down-arrow-white2x.png" width="36">
							</a>
						</div>
					</div>
				</div>
			<?php } else {
				$pgId = '';
				if(is_home()) {
					$pgId = get_option( 'page_for_posts' ); 
				}
				$bnrTtl = get_field('banner_title',$pgId);
				if(!$bnrTtl) {
					$bnrTtl = get_the_title();
				}
				$bnrDesc = get_field('banner_description',$pgId);
				$bnrImg = get_field('banner_image',$pgId);
				if(!$bnrImg) {
					$bnrImg = get_template_directory_uri().'/img/body-header-bg1x.png';
				} 
				
				?>
				<div class="body-header <?php if(is_page_template('pagetemplates/template-contact-us.php')){ echo "contheader";} ?><?php if(!is_page_template('pagetemplates/template-faq.php') && !is_page_template('pagetemplates/template-contact-us.php') && !is_page_template('pagetemplates/template-portfolio-archives.php') && !is_page_template('pagetemplates/template-portfolio-investments.php') && !is_page_template('pagetemplates/template-portfolio-managment.php') && !is_home() && !is_single() && !is_page_template('pagetemplates/template-portfolio.php') && !is_page_template('pagetemplates/template-landing.php') && !is_page_template('pagetemplates/template-landing-v2.php') && !is_page_template('pagetemplates/template-service-child-landing.php')){?>brochure<?php }?>" <?php if($bnrImg) { ?>style="background-image:url('<?php echo $bnrImg;?>');"<?php } ?>>
					
					<?php if(is_page_template('pagetemplates/template-service-child-landing.php')){?>
					
					
						<div class="imagesection">
						  <div class="imagesectiondiv">
							<div class="herodiv">
							   <h1><?php echo $bnrTtl; ?></h1>
							 <?php if($bnrDesc) { ?>
								<div class="headerp"><?php echo $bnrDesc; ?></div>
							 <?php } ?>
							</div>
						  </div>
						</div>					
					<?php } else { ?>
						
						<div class="content-div1180">
						<?php if(is_single()) { ?>
							<a class="back-to-blog-btn" data-ix="fade-on-load-3" href="<?php echo get_permalink(16);?>">
								<span class="back-arrow"><i class="fa fa-angle-left"></i></span> Back to News &amp; Views
							</a>
						<?php } ?>
						<div class="header-content-dv">
							<?php if(is_single()) { ?>
								<h1 data-ix="fade-on-load"><?php echo get_the_title();?></h1>
								<div class="post-details-div" data-ix="fade-on-load-2">
									<div class="publish-date"><?php echo get_the_date();?></div>
									<?php $category_detail=get_the_category(); 
									if($category_detail) { ?>
										<div class="post-categories">Categories: 
											<?php foreach($category_detail as $key => $value) { ?>
												<a class="categorie-link" href="<?php echo get_category_link($value->term_id);?>"><?php echo ($key!=(count($category_detail)-1)) ? $value->name.',' : $value->name;?></a>
											<?php } ?>
										</div>
									<?php } ?>
								</div>
							<?php } else { 
								$pgTtl = get_the_title();
								if(is_home()) {
									$pgTtl = get_the_title(get_option( 'page_for_posts' ));
								} ?>
									<div class="page-title"><?php echo $pgTtl;?></div>
									<?php if($bnrTtl) { ?>
										<h1 data-ix="fade-on-load"><?php echo $bnrTtl;?></h1>
								<?php } if(is_page_template('pagetemplates/template-our-services.php')) {
									if($bnrDesc) { ?>
										<div class="hero-paragraph" data-ix="fade-on-load-2"><?php echo $bnrDesc;?></div>
									<?php } 
								}
							}
							if(is_page_template('pagetemplates/template-our-services.php')) { 
								$bnrVdo = get_field('banner_video_url');
								$bnrVdoThmb = get_field('banner_video_thumb');
								if(!$bnrVdoThmb) {
									$bnrVdoThmb = 'https://i.vimeocdn.com/video/579078846_1280.jpg';
								}
								if($bnrVdo) {
									$vdoId = (int) substr(parse_url($bnrVdo, PHP_URL_PATH), 1);
									$ncodVdo = rawurlencode($bnrVdo);
									$ncodVdoThumb = rawurlencode($bnrVdoThmb);
									$plyrUrl = 'https://player.vimeo.com/video/'.$vdoId;
									$ncodPlyrUrl = rawurlencode($plyrUrl); ?>
									<div class="hero-btn-parent" data-ix="fade-on-load-2">
										<a class="home-video-link w-clearfix w-inline-block w-lightbox" href="#">
											<img class="play-icon" src="<?php echo get_template_directory_uri();?>/img/play-icon2x.png" width="38">
											<div>How We Work</div>
											<script class="w-json" type="application/json">{ "items": [{
												"type": "video",
												"url": "<?php echo $bnrVdo;?>",
												"html": "<iframe class=\"embedly-embed\" src=\"//cdn.embedly.com/widgets/media.html?src=<?php echo $ncodPlyrUrl;?>&url=<?php echo $ncodVdo;?>&image=<?php echo $ncodVdoThumb;?>&key=c4e54deccf4d4ec997a64902e9a30300&type=text%2Fhtml&schema=vimeo\" width=\"940\" height=\"529\" scrolling=\"no\" frameborder=\"0\" allowfullscreen></iframe>",
												"thumbnailUrl": "<?php echo $bnrVdoThmb;?>",
												"width": 940,
												"height": 529
												}] }
											</script>
										</a>
									</div>
								<?php } 
							}
							if(!is_home() && !is_single()) { ?>
								<a class="down-arrow-new w-inline-block" data-ix="fade-on-load-3" href="#">
									<img src="<?php echo get_template_directory_uri();?>/img/down-arrow-white2x.png" width="36">
								</a>
							<?php } 
							if(is_page_template('pagetemplates/template-landing-v2.php')) { ?>
								<div class="hero-paragraph" data-ix="fade-on-load-2"><?php echo get_the_excerpt();?></div>
							<?php } ?>							
						</div>
					</div>
					<?php } ?>
				</div>
			<?php } 
			
			if(!is_page_template('pagetemplates/template-our-services.php') && !is_page_template('pagetemplates/template-service-child-landing.php') ) {
				if($bnrDesc) { ?>
					<div class="hero-paragraph blah" data-ix="fade-on-load-2"><?php echo $bnrDesc;?></div>
				<?php }  
			} ?>