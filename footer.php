<?php echo  section_builder(); ?>
<?php $scrlHdrLogo = get_field('scroll_nav_logo','option');
if(!$scrlHdrLogo) {
	$scrlHdrLogo = get_template_directory_uri().'/img/responsive-nav-logo2x.png';
} 
$cpyTxt = get_field('copyrights','option');?>
		<div class="footer">
			<div class="sitemap-parent-flex" >
				<div class="sitemap-column-parent">
					<?php dynamic_sidebar('footer-widget'); ?> 
				</div>
			</div>
			<div class="footer-parent">
				<div class="content-div1180 w-clearfix">
					<img class="footer-icon" src="<?php echo $scrlHdrLogo;?>" width="40">
					<?php if($cpyTxt) { ?>
						<div><?php echo $cpyTxt;?></div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php if(!is_front_page()) { ?>
			</div> <!-- main-wrapper -->
		<?php } ?>

		<?php wp_footer(); ?>

	
			


	</body>
</html>
