<?php get_header(); ?>
<div class="body-section">
	<div class="post-content-div1180 w-row">
		<div class="post-left-col w-col w-col-2 w-col-small-small-stack">
			<h2><?php single_cat_title();?></h2>
		</div>
		<?php if (have_posts()): ?>
			<div class="post-middle-col w-col w-col-7 w-col-small-small-stack">
				<div class="w-row" data-ix="fade-on-scroll">
					<?php $i = 0;
					while (have_posts()) : the_post();?>
					<div class="w-col w-col-6">
						<div class="archive-col<?php echo ($i%2==0) ? 1 : 2;?> archive-post">
							<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="<?php echo get_permalink();?>">
								<?php if(has_post_thumbnail()) {
									$imgArr = wp_get_attachment_image_src(get_post_thumbnail_id(),'archive-post-thumb');
									if(isset($imgArr[0]) && $imgArr[0]!='') {
										$imgSrc = $imgArr[0];
									} else {
										$imgSrc = 'http://via.placeholder.com/622x402';
									}
								} else {
									$imgSrc = 'http://via.placeholder.com/622x402';
								}?>
								<img class="post-thumb" src="<?php echo $imgSrc; ?>">
								<div class="post-thumb-title"><?php echo get_the_title();?></div>
								<div class="post-thumb-date"><?php echo get_the_date();?></div>
							</a>
						</div>
					</div>
					<?php 
					if($i%2==1) {
						echo '</div><div class="w-row" data-ix="fade-on-scroll">';
					}
					$i++;
					endwhile;?>
				</div>
			</div>
		<?php endif; 
		$catList = get_categories('exclude='.get_query_var( 'cat' ));
		if($catList) { ?>
			<div class="post-right-col w-col w-col-3 w-col-small-small-stack">
				<div>
					<div class="w-row">
						<div class="w-col w-col-3"></div>
						<div class="w-col w-col-9">
							<div class="post-sidebar-heading">Category</div>
							<ul class="related-posts">
								<?php foreach($catList as $key => $value) { ?>
									<li class="related-post-listitem">
										<a href="<?php echo get_category_link($value->term_id);?>"><?php echo $value->name;?></a>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>


	<!-- <main role="main">
		
		<section>

			<h1><?php //_e( 'Categories for ', 'html5blank' ); single_cat_title(); ?></h1>

			<?php //get_template_part('loop'); ?>

			<?php //get_template_part('pagination'); ?>

		</section>
		
	</main> -->

<?php get_footer(); ?>
