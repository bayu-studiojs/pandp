<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7'); // Custom scripts
        wp_enqueue_script('bootstrap'); // Enqueue it!
		
		wp_register_script('ppathwayswebflow', get_template_directory_uri() . '/js/properties-and-pathways-v2.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('ppathwayswebflow'); // Enqueue it!


        wp_register_script('slicksliderjs', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('slicksliderjs'); // Enqueue it!
		
		wp_register_script('ppathway', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('ppathway'); // Enqueue it!
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.7', 'all');
    wp_enqueue_style('bootstrap'); // Enqueue it!
	
	wp_register_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0', 'all');
    wp_enqueue_style('font-awesome'); // Enqueue it!
	
	wp_register_style('ppathwayswebflow', get_template_directory_uri() . '/css/properties-and-pathways-v2.css', array(), '1.0', 'all');
    wp_enqueue_style('ppathwayswebflow'); // Enqueue it!


    wp_register_style('slickslidercss', get_template_directory_uri() . '/css/slick.css', array(), '1.0', 'all');
    wp_enqueue_style('slickslidercss'); // Enqueue it!
	
	wp_register_style('ppathway', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('ppathway'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
	register_sidebar(array(
        'name' => __('Footer Widget', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'footer-widget',
        'before_widget' => '<div id="%1$s" class="%2$s footer-column">',
        'after_widget' => '</div>',
        'before_title' => '<div class="footer-heading">',
        'after_title' => '</div>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
//add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('init', 'create_post_type_testimonials'); // Add Custom post type for testimonials
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}


/*------------------------------------*\
    Custom Post Type for Testimonials
\*------------------------------------*/

// Custom posts for Testimonials
function create_post_type_testimonials()
{
    register_post_type('testimonials', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Testimonials', 'testimonials'), // Rename these to suit
            'singular_name' => __('Testimonial', 'testimonial'),
            'add_new' => __('Add New', 'testimonial'),
            'add_new_item' => __('Add New Testimonial', 'testimonial'),
            'edit' => __('Edit', 'testimonial'),
            'edit_item' => __('Edit Testimonial', 'testimonial'),
            'new_item' => __('New Testimonial', 'testimonial'),
            'view' => __('View Testimonial', 'testimonial'),
            'view_item' => __('View Testimonial', 'testimonial'),
            'search_items' => __('Search Testimonial', 'testimonial'),
            'not_found' => __('No Testimonial found', 'testimonial'),
            'not_found_in_trash' => __('No Testimonial found in Trash', 'testimonial')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'rewrite'  => array( 'slug' => 'testimonials', 'with_front' => false ),
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'menu_icon'   => 'dashicons-testimonial',
        'taxonomies' => array() // Add Category and Post Tags support
    ));
}


/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	/* acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	)); */
	
}

/** UPDATE WP_ADMIN SECTION **/
add_action("login_head", "allworks_login_head");
function allworks_login_head() {
if(!is_user_logged_in()){ 
$admin_login_logo=get_field('admin_logo','option');
$admin_background_color=get_field('admin_background_color','option');
$admin_logo=($admin_login_logo!='') ? $admin_login_logo : get_stylesheet_directory_uri()."/img/favicon.Icon"; 
$admin_background_color=($admin_background_color!='') ? $admin_background_color : "#13161F"; 
echo"<style>
      .login #login h1 a,.login h1 a {
          background-image: url('".$admin_logo."') !important;
          height:106px !important;
          width: 320px !important;
          background-size:80% !important;
          background-position:center;
      }
      html,body{ background:".$admin_background_color." !important; }
     .login .message,.login form{ box-shadow:2px 0 4px 4px rgba(0, 0, 0, 0.1) !important; }
</style>";
}
}
/*UPDATE Favicon SECTION */
add_action('wp_head','favicon');
add_action( 'admin_head', 'favicon' );
add_action( 'login_head', 'favicon' );
function favicon() {
     $fav_icon=get_field('fav_icon','option');
     $favicon=($fav_icon!='') ? $fav_icon : get_stylesheet_directory_uri()."/img/favicon.Icon"; 
     if($favicon!='') echo '<link rel="icon" href="'.$favicon.'"/>';    
}

function all_year(){
 return date('Y')   ;
}
add_shortcode('year','all_year');
add_shortcode('socialicons','getSocialIcons');
function getSocialIcons() {
	$html = '';
	$sclIcns = get_field('social_share','option');
	if($sclIcns) {
		$html .= '<div class="social-icons-wrapper">';
		foreach($sclIcns as $key => $value) {
			$html .= '<a class="social-footer-btn" href="'.$value['social_link'].'">'.$value['social_icon'].'</a>';
		}
		$html .= '</div>';
	}
	return $html;
}
add_shortcode('contactdetails','getContactDetails');
function getContactDetails() {
	$html = '';
	$addrs = get_field('contact_address','option');
	$phone = get_field('contact_telephone','option');
	if($addrs) {
		if(isset($addrs[0]['address'])) {
			$addrs = $addrs[0]['address'];
		}
		$html .= '<div class="footer-address-details-div">
						<div class="address-label">A</div>
						<div>'.$addrs.'</div>
					</div>';
	}
	if($phone) {
		$html .= '<div class="footer-address-details-div">
			<div class="address-label">T</div>
			<div><a href="tel:'.$phone.'">'.$phone.'</a></div>
		</div>';
	}
	return $html;
}
function limit_words($string, $word_limit) {

	$words = explode(' ', $string);

	return implode(' ', array_slice($words, 0, $word_limit));

}
add_image_size('featured-post-thumb',632,402);
add_image_size('archive-post-thumb',622,402);
add_image_size('portfolio-post-thumb',485,450);
function getMorePost() {
	global $wpdb;
	$result = array();
	if(isset($_POST['paged']) && $_POST['paged']!='') {
		if(!isset($_POST['paged'])) {
			$paged = 1;
		} else {
			$paged = $_POST['paged']+1;
		}
		$args = array(
			'post_type' => 'post',
			'posts_per_page' =>8,
			'paged' => $paged
		);
		$excludeIds = $_POST['excludeIds'];
		if(isset($excludeIds) && count($excludeIds)>0) {
			$args['post__not_in'] = $excludeIds;
		}
		$wp_query = new WP_Query( $args );
		if( $wp_query->have_posts() ){
			$returnpost = $wp_query->post_count;
			$result['totalpost'] = $wp_query->found_posts;
			$i = 0;
			$result['content'] = '<div class="w-row" data-ix="fade-on-scroll">';
			while ( $wp_query->have_posts() ) : $wp_query->the_post();
				if($i%2==0) {
					$arcColVal = 'archive-col1';
				} else {
					$arcColVal = 'archive-col2';
				}
				if(has_post_thumbnail()) {
					$imgArr = wp_get_attachment_image_src(get_post_thumbnail_id(),'archive-post-thumb');
					if(isset($imgArr[0]) && $imgArr[0]!='') {
						$imgSrc = $imgArr[0];
					} else {
						$imgSrc = 'http://via.placeholder.com/622x402';
					}
				} else {
					$imgSrc = 'http://via.placeholder.com/622x402';
				}
				$result['content'] .= '<div class="w-col w-col-6">
					<div class="'.$arcColVal.' archive-post">
						<a class="post-thumbnail-parent w-inline-block" data-ix="fade-on-scroll" href="'.get_permalink().'">
							<img class="post-thumb" src="'.$imgSrc.'">
							<div class="post-thumb-title">'.get_the_title().'</div>
							<div class="post-thumb-date">'.get_the_date().'</div>
						</a>
					</div>
				</div>';
				if($i%2==1) {
					$result['content'] .= '</div><div class="w-row" data-ix="fade-on-scroll">';
				}
				$i++;
			endwhile;
			$result['content'] .='</div>';
		}
		wp_reset_query();	
	}
	echo json_encode($result);
	exit;
}
add_action( 'wp_ajax_nopriv_getMorePost', 'getMorePost' );  
add_action( 'wp_ajax_getMorePost', 'getMorePost' );


/***************Custom Post type**************/
function custom_post_type() {

 $custom_post_type_array = array(
         array('post_type' => 'team',    'singular_name' => 'Team',   'plural_name' => 'Team'),
		 array('post_type' => 'property',    'singular_name' => 'Property',   'plural_name' => 'Properties')
        );
 
 foreach($custom_post_type_array as $key => $value) {
  $labels = array(
   'name'                  => _x( $value['plural_name'], 'Post Type General Name', 'text_domain' ),
   'singular_name'         => _x( $value['singular_name'], 'Post Type Singular Name', 'text_domain' ),
   'menu_name'             => __( $value['plural_name'], 'text_domain' ),
   'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
   'parent_item_colon'     => __( 'Parent '.$value['singular_name'].':', 'text_domain' ),
   'all_items'             => __( 'All '.$value['plural_name'].'', 'text_domain' ),
   'add_new_item'          => __( 'Add New '.$value['singular_name'].'', 'text_domain' ),
   'add_new'               => __( 'New '.$value['singular_name'].'', 'text_domain' ),
   'new_item'              => __( 'New Item', 'text_domain' ),
   'edit_item'             => __( 'Edit '.$value['singular_name'].'', 'text_domain' ),
   'update_item'           => __( 'Update '.$value['singular_name'].'', 'text_domain' ),
   'view_item'             => __( 'View '.$value['singular_name'].'', 'text_domain' ),
   'search_items'          => __( 'Search '.$value['singular_name'].'', 'text_domain' ),
   'not_found'             => __( 'No '.strtolower($value['singular_name']).' found', 'text_domain' ),
   'not_found_in_trash'    => __( 'No '.strtolower($value['singular_name']).' found in Trash', 'text_domain' ),
   'items_list'            => __( 'Items list', 'text_domain' ),
   'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
   'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
   'label'                 => __( $value['singular_name'], 'text_domain' ),
   'description'           => __( $value['singular_name'].' information pages', 'text_domain' ),
   'labels'                => $labels,
   'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields', ),
   'hierarchical'          => false,
   'public'                => true,
   'show_ui'               => true,
   'show_in_menu'          => true,
   'menu_position'         => 5,
   'show_in_admin_bar'     => true,
   'show_in_nav_menus'     => true,
   'can_export'            => true,
   'has_archive'           => true,  
   'exclude_from_search'   => false,
   'publicly_queryable'    => true,
   'capability_type'       => 'page',
  );
  register_post_type( $value['post_type'], $args );
 }
}
add_action( 'init', 'custom_post_type', 0 );

/** SECTION BUILDER **/
function section_builder($postid='',$set_id = 0){
	global $post;
	$post_id=($postid!='') ? $postid :  $post->ID; 
	if(is_home() || is_single()) {
		$post_id = get_option( 'page_for_posts' );
	}
	$page_builder=get_field('page_builder',$post_id);
	$container_wrapper='';
	if( have_rows('page_builder',$post_id) ):
		$layout='';
		
		while ( have_rows('page_builder',$post_id) ) : the_row();  $layout='';
			$wrapper='';
			$layout=get_row_layout();
			
			if( $layout == 'contact_section' ):
			    $description = get_field('description','options');
				$page_link   = get_field('page_link','options');
				$page_link_2   = get_field('page_link_2','options');
				if(!empty($description)){
					echo '<div class="cta-section"><div class="content-div1180">';
					echo '<div class="cta-text">'.get_field('description','options').'</div>';
					if(!empty($page_link)){
						echo '<a class="cta-button w-button" href="'.$page_link_2.'">'.get_field("learn_more_title","options").'<span class="contact-arrow">›</span></a><a class="cta-button w-button" href="'.$page_link.'">'.get_field("contact_us_title","options").'<span class="contact-arrow">›</span></a>';
					}
					echo '</div></div>';
				}
			endif;
			
       endwhile;
	   
    endif;

	echo $container_wrapper;
}


add_filter( 'gform_validation_message_1', 'change_message', 10, 2 );
function change_message( $message, $form ) {
    return "<div class='validation_error'>Oops! Something went wrong while submitting the form.</div>";
}
function my_acf_set_repeater( $value, $post_id, $field ){

    // this one should consists array of the names 
   if ($value === false) {
        $value = array(
             array(
                'field_59b0c6aac5f21' => 'Title Description ',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Purchase Date',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Purchase Price',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Return in Year 1',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Tenants',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Type',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Lease Term',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Options',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Size',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Lettable Area',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Market Value Date',
                'field_59b0c6c2c5f22' => ''
			),
			array(
                'field_59b0c6aac5f21' => 'Market Value',
                'field_59b0c6c2c5f22' => ''
			)
        );
    }
    return $value;

}
add_filter('acf/load_value/key=field_59b0c66bc5f20', 'my_acf_set_repeater', 10, 3); 
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
add_post_type_support( 'page', 'excerpt' );
add_filter( 'gform_confirmation_anchor_4', '__return_true' );
/**
 * Populate ACF select field options with Gravity Forms forms
 */
function acf_populate_gf_forms_ids( $field ) {
	if ( class_exists( 'GFFormsModel' ) ) {
		$choices = [];
		$choices[''] = 'Select a form';
		foreach ( \GFFormsModel::get_forms() as $form ) {
			$choices[ $form->id ] = $form->title;
		}
		$field['choices'] = $choices;
	}
	return $field;
}
add_filter( 'acf/load_field/key=field_5d67c75da3789', 'acf_populate_gf_forms_ids' );

add_filter( 'gform_field_validation', 'validate_phone', 10, 4 );
function validate_phone( $result, $value, $form, $field ) {
    $pattern = "/^(\+44\s?7\d{3}|\(?07\d{3}\)|\(?01\d{3}\)?)\s?\d{3}\s?\d{3}$/";
    if ( $field->type == 'phone' && $field->phoneFormat != 'standard' && ! preg_match( $pattern, $value ) ) {
        $result['is_valid'] = false;
        $result['message']  = 'Please enter a valid phone number.';
    }
 
    return $result;
}

add_filter( 'gform_field_validation', function ( $result, $value, $form, $field ) {
    if ( $field->type == 'name' ) {
 
        // Input values
        $prefix = rgar( $value, $field->id . '.2' );
        $first  = rgar( $value, $field->id . '.3' );
        $middle = rgar( $value, $field->id . '.4' );
        $last   = rgar( $value, $field->id . '.6' );
        $suffix = rgar( $value, $field->id . '.8' );
 
        if ( empty( $prefix ) && ! $field->get_input_property( '2', 'isHidden' )
             || empty( $first ) && ! $field->get_input_property( '3', 'isHidden' )
             || empty( $middle ) && ! $field->get_input_property( '4', 'isHidden' )
             || empty( $last ) && ! $field->get_input_property( '6', 'isHidden' )
             || empty( $suffix ) && ! $field->get_input_property( '8', 'isHidden' )
        ) {
            $result['is_valid'] = false;
            $result['message']  = empty( $field->errorMessage ) ? __( 'This field is required. Please enter a complete name.', 'gravityforms' ) : $field->errorMessage;
        } else {
            $result['is_valid'] = true;
            $result['message']  = '';
        }
    }
 
    return $result;
}, 10, 4 );

add_filter( 'gform_confirmation_anchor', '__return_true' );
?>
