<?php get_header(); ?>

<div class="body-section">
	<div class="post-content-div1180 w-row">
		<div class="w-col w-col-12">
			<main role="main">
				<!-- section -->
				<section>

					<!-- article -->
					<article id="post-404">
						<img src="<?php echo get_template_directory_uri();?>/img/404.png" />
						<h2>OOPS, Sorry something went wrong.</h2>
						<h3>Either the link you’d followed was wrong, or the page doesn’t exist anymore</h3>
					</article>
					<!-- /article -->

				</section>
				<!-- /section -->
			</main>
		</div>
	</div>
</div>
<?php get_footer(); ?>
